using System.Collections;
using UnityEngine.Events;
using UnityEngine;


public class EnergyManager : MonoBehaviour
{
    #region Var
    private static EnergyManager Singleton;
    [SerializeField,Tooltip("Energia massima del Giocatore")]
    int MaxEnergy;
    [SerializeField,Tooltip("Minuti necessari per ottenere un unit� di energia")]
    int EnergyTimer;
    public int levelExp;
    int Energy;  
    int Money;
    int Exp;
    int Time;
    int Extra;
    Coroutine Test;
    UnityEvent UpdateText;
    #endregion

    #region UnityCallbacks
    private void Start()
    {
        if (PlayerPrefs.GetInt("start") == 0)
        {
            Money = 600;
            Save();
            PlayerPrefs.SetInt("start", 1);
        }
        Inizialization();
        Check();
        Test= StartCoroutine(EnergyUpdate());
    }

    private void OnDestroy()
    {
        StopCoroutine(Test);
    }
    #endregion
    void Inizialization()
    {
        if (!Singleton) Singleton = this;
        Energy = PlayerPrefs.GetInt("Energy");
        Extra = PlayerPrefs.GetInt("Extra");
        Money = PlayerPrefs.GetInt("Money");
        Exp = PlayerPrefs.GetInt("Exp");
        DontDestroyOnLoad(Singleton);
    }

    IEnumerator EnergyUpdate()
    {
        yield return new WaitForSeconds(EnergyTimer * 60f);
        Check();
        StartCoroutine(EnergyUpdate());
    }

    int Check()
    {
        Time += (System.DateTime.Now.Year - 2021) * 365;
        Time += System.DateTime.Now.DayOfYear;
        Time *= 24;
        Time += System.DateTime.Now.Hour;
        Time *= 60;
        Time += System.DateTime.Now.Minute;
        var Difference = Time - PlayerPrefs.GetInt("Time");
  
        Energy += Difference/EnergyTimer;
        if (Energy > MaxEnergy)
        {
            Energy = MaxEnergy;
            Extra = 0;
        }
        Extra += Difference % EnergyTimer;
        if (Extra / EnergyTimer > 0)
        {
            Energy += Extra / EnergyTimer;
            Extra -= EnergyTimer;
            if (Energy > MaxEnergy)
            {
                Energy = MaxEnergy;
                Extra = 0;
            }
        }
        Save();
        PlayerPrefs.SetInt("Time", Time);
        Time = 0;
        return Difference;
    }

    public static int GetEnergy()
    {
        return Singleton.Energy;
    }
    public static int GetMaxEnergy()
    {
        return Singleton.MaxEnergy;
    }
    public static int GetMoney()
    {
        return Singleton.Money;
    }
    public static int GetExp()
    {
        return Singleton.Exp;
    }
    public static bool UseEnergy(int ToUse)
    {
        if (Singleton.Energy - ToUse < 0)
        {
            return false;
        }
        else 
        {
            Singleton.Energy -= ToUse;
            Singleton.Save();
            return true;
        }
    }
    public static bool UseMoney(int ToUse)
    {
        if (Singleton.Money - ToUse < 0)
        {
            return false;
        }
        else
        {
            Singleton.Money -= ToUse;
            Singleton.Save();
            return true;
        }
    }
    public static void GainExp(int ToGain)
    {
        Singleton.Exp += ToGain;
        Singleton.Save();
    }
    public static int GetLevel()
    {
        return Singleton.Exp;
    }
    void Save()
    {
        PlayerPrefs.SetInt("Energy", Energy);
        PlayerPrefs.SetInt("Extra", Extra);
        PlayerPrefs.SetInt("Money", Money);
        PlayerPrefs.SetInt("Exp", Exp);
        UpdateText?.Invoke();
    }

    public static int ExpToLevel()
    {
        int Level = Singleton.Exp / Singleton.levelExp;
        return Level;
    }
    public static int GetExpForLevel()
    {
        return Singleton.levelExp;
    }
    public static void Reset()
    {
        PlayerPrefs.SetInt("Energy", Singleton.MaxEnergy);
        PlayerPrefs.SetInt("Extra", 0);
        PlayerPrefs.SetInt("Money", 600);
        PlayerPrefs.SetInt("Exp", 0);
        PlayerPrefs.SetInt(Quadri.IlBacio.ToString() + "index", 0);
        PlayerPrefs.SetInt(Quadri.PalaDiMontefeltro.ToString() + "index", 0);
        PlayerPrefs.SetInt(Quadri.SposalizioDelleVergine.ToString() + "index", 0);
        PlayerPrefs.SetInt(Quadri.IlBacio.ToString(), 0);
        PlayerPrefs.SetInt(Quadri.PalaDiMontefeltro.ToString(), 0);
        PlayerPrefs.SetInt(Quadri.SposalizioDelleVergine.ToString(), 0);
        Singleton.Energy = PlayerPrefs.GetInt("Energy");
        Singleton.Exp = PlayerPrefs.GetInt("Exp");
        Singleton.Money = PlayerPrefs.GetInt("Money");
        Singleton.Extra = PlayerPrefs.GetInt("Extra");

        for (int i = 0; i < 3; i++)
        {
            PlayerPrefs.SetInt(Quadri.SposalizioDelleVergine.ToString() + "funFact" + i, 0);
            PlayerPrefs.SetInt(Quadri.PalaDiMontefeltro.ToString() + "funFact" + i, 0);
            PlayerPrefs.SetInt(Quadri.SposalizioDelleVergine.ToString() + "funFact" + i, 0);
        }
    }
}
