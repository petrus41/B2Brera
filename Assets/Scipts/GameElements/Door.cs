using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField]
    int ExpToOpen;

    private void Update()
    {
        if(EnergyManager.ExpToLevel()>=ExpToOpen)
        {
            gameObject.SetActive(false);
        }
    }
}
