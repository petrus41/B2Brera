using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField, Tooltip("Velocit� del player")]
    float Speed;
    [SerializeField, Tooltip("Velocit� di rotazione del giocatore")]
    float RotationSpeed;
    [SerializeField, Tooltip("Distanza da percorrere del player")]
    float Distance;
    [SerializeField, Tooltip("UI gestione menu")]
    GameMenu UI;
    [HideInInspector]
    public Vector3 BeginTranslation;
    Vector3 EndTranslation;
    [HideInInspector]
    public Quaternion BeginRotation;
    Quaternion EndRotation;
    int Direction;
    int RotatingDirection;
    bool HasToMoveTo = false;
    bool Moving = false;
    bool Rotating = false;
    float Timer = 0;
    [HideInInspector]
    public Paint ActivePaint;


    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount >= 1)
        {
            // The pos of the touch on the screen
            Vector2 vTouchPos = Input.GetTouch(0).position;

            // The ray to the touched object in the world
            Ray ray = Camera.main.ScreenPointToRay(vTouchPos);

            // Your raycast handling
            RaycastHit vHit;
            if (Physics.Raycast(ray.origin, ray.direction, out vHit))
            {
                if (vHit.transform.tag == "Paint")
                {
                    Paint Paint = vHit.transform.gameObject.GetComponent<Paint>();
                    if (!Paint.isFocused)
                    {
                        //UI.Activate(false, true, false, false, false);
                        GetComponent<BoxCollider>().enabled = false;
                        MoveTo(Paint.LookingPosition, Paint.LookingAngle);
                        Paint.isFocused = true;
                        vHit.transform.gameObject.GetComponent<Collider>().enabled = false;
                        ActivePaint = Paint;
                        UI.OpenPaintMenu();
                    }
                   
                }
            }
        }
        if (Moving)
        {
            transform.Translate(Vector3.forward * Direction * Speed * Time.deltaTime);
            Timer += Time.deltaTime;
            if (Timer > Distance / Speed)
            {
                Stop();
            }
        }
        else if (Rotating)
        {
            transform.Rotate(0, RotationSpeed * RotatingDirection * Time.deltaTime, 0);
        }
        else if (HasToMoveTo)
        {
            transform.position = Vector3.Lerp(BeginTranslation, EndTranslation, Timer);
            transform.rotation = Quaternion.Lerp(BeginRotation, EndRotation, Timer);
            Timer += Time.deltaTime;
            if (Timer >= 1)
            {
                Stop();
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Stop();
        transform.Translate(-Vector3.forward*Direction*0.15f);
    }
    public void Move(int Orientation)
    {
        Direction = Orientation;
        Moving = true;
    }
    public void Stop()
    {
        Moving = false;
        HasToMoveTo = false;
        GetComponent<BoxCollider>().enabled = true;
        Timer = 0;
    }
    public void Turn(int Orientation)
    {
        RotatingDirection = Orientation;
        Rotating = true;
    }
    public void Stay()
    {
        Rotating = false;   
    }
    public void MoveTo(Vector3 Destination, Vector3 Degree)
    {
        EndTranslation = Destination;
        EndRotation = Quaternion.Euler(Degree);
        BeginTranslation = transform.position;
        BeginRotation = transform.rotation;
        HasToMoveTo = true;
    }
}
