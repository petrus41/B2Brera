using UnityEngine.UI;
using UnityEngine;

public enum Quadri
{
    None=-1,
    PalaDiMontefeltro,
    SposalizioDelleVergine,
    IlBacio,
}
public class Paint : MonoBehaviour
{
    Vector3 ExitLocation;
    [SerializeField, Tooltip("Posizione da cui si guarda il quadro")]
    public Vector3 LookingPosition;
    [SerializeField, Tooltip("Angolazione da cui si guarda il quadro")]
    public Vector3 LookingAngle;
    [SerializeField, Tooltip("Player")]
    GameObject Player;
    [SerializeField, Tooltip("Menu da nascondere")]
    GameObject MoveMenu;
    [SerializeField, Tooltip("Menu da Mostrare")]
    GameObject ExitMenu;
    [SerializeField]
    public string[] FunFacts;
    [HideInInspector]
    public bool isFocused;
    [HideInInspector]
    public bool isLocked;
    [Tooltip("Nome dell'opera")]
    public Quadri Opera;
    public GameObject Quest;
    public int ExpValue;
    public int MoneyValue;
    [Tooltip("Aspetti dei quadri")]
    public Sprite[] Aspect;

    public void Update()
    {
        GetComponent<SpriteRenderer>().sprite = Aspect[PlayerPrefs.GetInt(Opera.ToString()+"index")];
        if (PlayerPrefs.GetInt(Opera.ToString()) == 0)
        {
            isLocked = true;
            Quest.SetActive(true);
        }
        else 
        {
            isLocked = false;
            Quest.SetActive(false);
        }
    }

    public void UnFocus()
    {
        isFocused = false;
        GetComponent<BoxCollider>().enabled = true;
    }
}
