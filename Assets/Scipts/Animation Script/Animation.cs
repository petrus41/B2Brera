using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation : MonoBehaviour
{ public Sprite Locket;
    public GameObject LocketPrefab;
    public GameObject LocketAnimationPrefab;
    public GUI FunFact;
    // Start is called before the first frame update
    void Start()
    {   
      
        
    }

    // Update is called once per frame
    void Update()
    {
          if (Input.GetKeyDown(KeyCode.Space))
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            Destroy(LocketPrefab);
            GameObject AnimationPrefab = Instantiate(LocketAnimationPrefab);
            AnimationPrefab.transform.position = transform.position;
            Destroy(AnimationPrefab, 0.8f);
            Instantiate(Locket);
        }
       
    }  


    
}
