using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocialScripts : MonoBehaviour
{
    [SerializeField]
    GameObject MainMenu;
    public void Open(string URL)
    {
        Application.OpenURL(URL);
    }

    public void Back()
    {
        MainMenu.SetActive(true);
        gameObject.SetActive(false);
    }
}
