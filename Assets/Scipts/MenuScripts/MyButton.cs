using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class MyButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField,Tooltip("Player")]
    Movement Player;
    [SerializeField, Tooltip("Direzione di rotazione del player (1 verso destra, -1 verso sinistra)")]
    int Direction;

    public void OnPointerDown(PointerEventData eventData)
    {
        Player.Turn(Direction);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Player.Stay();
    }
}
