
using UnityEngine;

public enum Risposta
{
    None=-1,
    Pala,
    Bacio,
    Sposalizio,
};
public class Risposte : MonoBehaviour
{
    public GameMenu UI;
    public Risposta Risposta;

    public void Send()
    {
        UI.Reply = Risposta;
    }
}
