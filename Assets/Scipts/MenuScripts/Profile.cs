using UnityEngine;
using UnityEngine.UI;

public class Profile : MonoBehaviour
{
    [SerializeField]
    Text PlayerName;
    [SerializeField]
    GameObject Setter;
    [SerializeField]
    Image ProfilePic;
    [SerializeField]
    Image ProfilePicGame;
    [SerializeField]
    Sprite[] Images;
    // Start is called before the first frame update
   

    // Update is called once per frame
    void Update()
    {
        PlayerName.text = PlayerPrefs.GetString("Name");
        ProfilePic.sprite = Images[PlayerPrefs.GetInt("Profile")];
        ProfilePicGame.sprite = Images[PlayerPrefs.GetInt("Profile")];
    }
    public void SaveName()
    {
        Setter.SetActive(false);
    }
    public void OnClick()
    {
        Setter.SetActive(true);
    }
    public void SetPic(int index)
    {
        PlayerPrefs.SetInt("Profile", index);
    }
}
