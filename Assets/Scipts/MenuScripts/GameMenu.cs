using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;
public class GameMenu : MonoBehaviour
{
    [SerializeField, Tooltip("Player GameObject")]
    Movement Player;
    [SerializeField, Tooltip("Menu di movimento")]
    GameObject MoveMenu;
    [SerializeField, Tooltip("Menu dei quadri")]
    GameObject PaintMenu;
    [SerializeField, Tooltip("Menu della vittoria")]
    GameObject WinMenu;
    [SerializeField, Tooltip("Menu del profilo")]
    GameObject ProfileMenu;
    [SerializeField, Tooltip("Menu Minigioco")]
    GameObject MiniGameMenu;
    [SerializeField, Tooltip("PopUp Uscita")]
    GameObject ExitPopUp;
    [SerializeField, Tooltip("PopUp di uscita dal gioco")]
    GameObject ExitMiniGamePopUp;
    [SerializeField, Tooltip("PopUp consiglio")]
    GameObject HintPopUp;
    [SerializeField, Tooltip("PopUp Gioco")]
    GameObject GamePopUp;
    [SerializeField, Tooltip("PopUp no")]
    GameObject NoPopUp;
    [SerializeField, Tooltip("PopUp Fail")]
    GameObject FailPopUp;
    [SerializeField, Tooltip("NoMoreHint")]
    GameObject NoMoreHint;
    [SerializeField, Tooltip("Energy")]
    GameObject EnergyPopUP;
    [SerializeField, Tooltip("FunFactMenu")]
    GameObject FunFactMenu;
    [HideInInspector]
    public Risposta Reply;


    public UnityEvent ExitPaintEvent;

    public void OnClickForward()
    {
        Player.Move(1);
    }
    public void OnClickBackward()
    {
        Player.Move(-1);
    }
    public void ExitPaint()
    {
        Player.MoveTo(Player.BeginTranslation, Player.BeginRotation.eulerAngles);
        Activate(true,false,false,false,false);
        HidePopUP();
        ExitPaintEvent?.Invoke();
    }
    public void UseEnergy(int ToUse)
    {
        if (!EnergyManager.UseEnergy(ToUse))
        {
            print("You have no energy");
        }
    }
    public void OpenMainMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }
    public void OpenProfile()
    {
        Activate(false,false,false,true,false);
    }
    public void OpenGameMenu()
    {
        Activate(true,false,false,false,false);    
    }
    public void OpenExitPopUp()
    {
        ActivatePopUp(false, true, false, false);    
    }
    public void Activate(bool GameUI,bool PaintUI,bool WinUI,bool ProfileUI,bool MiniGameUI)
    {
        MoveMenu.SetActive(GameUI);
        PaintMenu.SetActive(PaintUI);
        WinMenu.SetActive(WinUI);
        ProfileMenu.SetActive(ProfileUI);
        MiniGameMenu.SetActive(MiniGameUI);
    }
    public void ActivatePopUp(bool minigame,bool exit, bool hint, bool exitmini)
    {
        GamePopUp.SetActive(minigame);
        ExitPopUp.SetActive(exit);
        HintPopUp.SetActive(hint);
        ExitMiniGamePopUp.SetActive(exitmini);
    }
    public void HidePopUP()
    {
        GamePopUp.SetActive(false);
        ExitPopUp.SetActive(false);
        HintPopUp.SetActive(false);
        ExitMiniGamePopUp.SetActive(false);
        NoPopUp.SetActive(false);
        FailPopUp.SetActive(false);
        NoMoreHint.SetActive(false);
        EnergyPopUP.SetActive(false);
    }
    public void PlayGame(int toUse)
    {
        HidePopUP();
        if (EnergyManager.UseEnergy(toUse))
        {
            Activate(false, false, false, false, true);
        }
        else
        {
            NoPopUp.SetActive(true);
        }
    }
    public void OpenExitMiniPopUp()
    {
        ActivatePopUp(false,false,false,true);
    }
    public void OpenPaintMenu()
    {
        HidePopUP();
        Activate(false,true,false,false,false);
        if (!Player.ActivePaint.isLocked)
        {
            FunFactMenu.SetActive(true);
        }
        else 
        {
            FunFactMenu.SetActive(false);
        }
    }
    public void OpenHintPopUp() 
    {
        ActivatePopUp(false, false, true, false);
    }
    public void UseHint()
    {
        int Money=0;
        switch (PlayerPrefs.GetInt(Player.ActivePaint.Opera.ToString() + "index"))
        {
            case 0: Money = 100;
                break;
            case 1: Money = 200;
                break;
            case 2: Money = 300;
                break;
            default:
                break;
        }
        if(EnergyManager.UseMoney(Money))
        {
            if (PlayerPrefs.GetInt(Player.ActivePaint.Opera.ToString() + "index") +1<=3)
            {
                PlayerPrefs.SetInt(Player.ActivePaint.Opera.ToString() + "index", PlayerPrefs.GetInt(Player.ActivePaint.Opera.ToString() + "index") + 1);

                HidePopUP();
            }
            else
            {
                NoMoreHint.SetActive(true);
            }
        }
        else 
        {
            HidePopUP();
            NoPopUp.SetActive(true);
        }
    }
    public void OpenGamePopUp()
    {
        if (Player.ActivePaint.isLocked)
        { 
            ActivatePopUp(true, false, false, false);
        }
    }
    public void Rispondere()
    {
        bool Corretta=false;
        switch (Player.ActivePaint.Opera)
        {
            case Quadri.IlBacio:
                switch (Reply) 
                { 
                    case Risposta.Pala: 
                        break;
                    case Risposta.Bacio:        
                        Corretta = true;
                        break;
                    case Risposta.Sposalizio:
                        break;
                default:
                        break;
                }
                break;
            case (Quadri.PalaDiMontefeltro):
                switch (Reply)
                {
                    case Risposta.Pala:
                        Corretta = true;
                        break;
                    case Risposta.Bacio:
                        break;
                    case Risposta.Sposalizio:
                        break;
                    default:
                        break;
                }
                break;
            case Quadri.SposalizioDelleVergine:
                switch (Reply)
                {
                    case Risposta.Bacio:
                        break;
                    case Risposta.Pala:
                        break;
                    case Risposta.Sposalizio:
                        Corretta = true;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        if (Corretta)
        {
            Activate(false, false, true, false, false);
            PlayerPrefs.SetInt(Player.ActivePaint.Opera.ToString() + "index", 4);
            PlayerPrefs.SetInt(Player.ActivePaint.Opera.ToString(), 1);
            EnergyManager.UseMoney(-Player.ActivePaint.MoneyValue);
            EnergyManager.GainExp(Player.ActivePaint.ExpValue);
        }
        else 
        {
            OpenPaintMenu();
            FailPopUp.SetActive(true);
        }
    }
    public void ShowEnergyPopUp()
    {
        EnergyPopUP.SetActive(true);
    }
}
