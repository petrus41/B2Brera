using UnityEngine.SceneManagement;
using UnityEngine;

public class Logo : MonoBehaviour
{

    float Timer;


    // Update is called once per frame
    void Update()
    {
        Timer += Time.deltaTime;
        if (Timer>=1f)
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
