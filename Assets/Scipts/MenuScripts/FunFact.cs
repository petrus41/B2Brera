using UnityEngine.UI;
using UnityEngine;

public class FunFact : MonoBehaviour
{
    [SerializeField]
    GameObject PopUp;
    [SerializeField]
    GameObject NoPopUP;
    [SerializeField]
    Movement Player;
    [SerializeField]
    Text FunFuctDisplay;
    public GameObject[] Unlocked;
    public GameObject[] Locked;
    int unlocking;
    public GameObject Display;


    private void Update()
    {
        switch (Player.ActivePaint.Opera)
        {
            case Quadri.IlBacio:
                for (int i = 0; i < 3; i++)
                {
                    if (PlayerPrefs.GetInt(Quadri.IlBacio.ToString() + "funFact" + i) == 1)
                    {
                        Locked[i].SetActive(false);
                        Unlocked[i].SetActive(true);
                    }
                    else 
                    {
                        Locked[i].SetActive(true);
                        Unlocked[i].SetActive(false);
                    }
                }
                break;
            case Quadri.SposalizioDelleVergine:
                for (int i = 0; i < 3; i++)
                {
                    if (PlayerPrefs.GetInt(Quadri.SposalizioDelleVergine.ToString() + "funFact" + i) == 1)
                    {
                        Locked[i].SetActive(false);
                        Unlocked[i].SetActive(true);
                    }
                    else
                    {
                        Locked[i].SetActive(true);
                        Unlocked[i].SetActive(false);
                    }
                }
                break;
            case Quadri.PalaDiMontefeltro:
                for (int i = 0; i < 3; i++)
                {
                    if (PlayerPrefs.GetInt(Quadri.PalaDiMontefeltro.ToString() + "funFact" + i) == 1)
                    {
                        Locked[i].SetActive(false);
                        Unlocked[i].SetActive(true);
                    }
                    else
                    {
                        Locked[i].SetActive(true);
                        Unlocked[i].SetActive(false);
                    }
                }
                break;
            default:
                break;
        }
    }

    public void TryUnlock(int Index)
    {
        PopUp.SetActive(true);
        unlocking = Index;
    }

    public void Hide()
    {
        PopUp.SetActive(false);
        Display.SetActive(false);
    }
    public void OnDisable()
    {
        PopUp.SetActive(false);
    }
    public void Unlock()
    {
        if(EnergyManager.UseMoney(100))
        {
            PlayerPrefs.SetInt(Player.ActivePaint.Opera.ToString() + "funFact" + unlocking, 1);
        }
        else 
        {
            NoPopUP.SetActive(true);
        }
        PopUp.SetActive(false);
    }
    public void ShowFunFact(int index)
    {
        Display.SetActive(true);
        FunFuctDisplay.text = Player.ActivePaint.FunFacts[index];
    }
}
