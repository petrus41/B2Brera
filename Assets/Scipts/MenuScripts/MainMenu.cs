using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    GameObject SocialMenu;
    public void Play()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
        
    }
    public void exit()
    {
        Application.Quit();
    }

    public void Social()
    {
        SocialMenu.SetActive(true);
        gameObject.SetActive(false);
    }

}
