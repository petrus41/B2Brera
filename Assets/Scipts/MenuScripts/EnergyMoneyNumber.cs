using UnityEngine.UI;
using UnityEngine;

public enum type
{
    none=-1,
    Money,
    Energy,
    Exp,
    Level,
    ExpReward,
    MoneyReward,
    Title,
}
public class EnergyMoneyNumber : MonoBehaviour
{
    public type Gender;
    public Movement Player;
    public string[] Titoli;

    // Update is called once per frame
    void Update()
    {
        switch (Gender)
        {
            case type.Energy:
                GetComponent<Text>().text = EnergyManager.GetEnergy() + "/" + EnergyManager.GetMaxEnergy();
                break;
            case type.Exp:
                GetComponent<Slider>().value = EnergyManager.GetExp()%EnergyManager.GetExpForLevel();
                GetComponent<Slider>().maxValue= EnergyManager.GetExpForLevel();
                break;
            case type.Money:
                GetComponent<Text>().text = EnergyManager.GetMoney().ToString();
                break;
            case type.Level:
                GetComponent<Text>().text = (EnergyManager.ExpToLevel()+1).ToString();
                break;
            case type.ExpReward:
                GetComponent<Text>().text = Player.ActivePaint.ExpValue.ToString();
                break;
            case type.MoneyReward:
                GetComponent<Text>().text = Player.ActivePaint.MoneyValue.ToString();
                break;
            case type.Title:
                switch(EnergyManager.ExpToLevel())
                {
                    case 1:GetComponent<Text>().text = "Neofita";
                        break;
                    case 2:
                        GetComponent<Text>().text = "Apprendista delle forme";
                        break;
                    case 3:
                        GetComponent<Text>().text = "Esperto Pittore e Scultore";
                        break;
                    case 4:
                        GetComponent<Text>().text = "Gran maestro d�arte";
                        break;
                }
                break;
        }
    }
}
